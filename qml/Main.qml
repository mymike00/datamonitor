import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
//import Ubuntu.PushNotifications 0.1
import Installdaemon 1.0

import QtQuick.LocalStorage 2.0
import "Storage.js" as Storage
import "DateUtils.js" as DateUtils

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'datamonitor.matteobellei'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

//    property string themeColor
    property string globalTheme: Theme.name;
    property string gridColor;
    property string bkgColor;
    property string textColor;
    property string fontColor;
    property string lineColor;
    property string buttonColor;
    property string barColor;
    property string nowTime
    property double todayStoredSIM : 0;
    property double totalStoredSIM : 0;
    property double todayStoredWIFI : 0;
    property double totalStoredWIFI : 0;

    property var colors: [Qt.rgba(0,0,0), Qt.rgba(0,0,1), Qt.rgba(0.37,0.37,0.37), Qt.rgba(0,0.6,0), Qt.rgba(1,0,0), Qt.rgba(1,0,1), Qt.rgba(0,1,1), Qt.rgba(1,1,0.2), Qt.rgba(1,1,1)]

    property var customColors: [settings.backgrndColorCust, settings.fontColorCust, settings.lineColorCust, settings.buttonColorCust, settings.gridColorCust, settings.barColorCust]
    property var opacArray: [settings.opac1, settings.opac2, settings.opac3, settings.opac4, settings.opac5, settings.opac6]
    property var customColorsIndex: [settings.backgroundColorIndex, settings.fontColorIndex, settings.lineColorIndex, settings.buttonColorIndex, settings.gridColorIndex, settings.barColorIndex]
    function updateOpac() {
        settings.opac1 = opacArray[0]
        settings.opac2 = opacArray[1]
        settings.opac3 = opacArray[2]
        settings.opac4 = opacArray[3]
        settings.opac5 = opacArray[4]
        settings.opac6 = opacArray[5]
        themeColorSelection()
    }
    function updateCustomColors() {
        settings.backgrndColorCust = customColors[0]
        settings.fontColorCust = customColors[1]
        settings.lineColorCust = customColors[2]
        settings.buttonColorCust = customColors[3]
        settings.gridColorCust = customColors[4]
        settings.barColorCust = customColors[5]
        themeColorSelection()
    }
    function updateCustomColorsIndex() {
        settings.backgroundColorIndex = customColorsIndex[0]
        settings.fontColorIndex = customColorsIndex[1]
        settings.lineColorIndex = customColorsIndex[2]
        settings.buttonColorIndex = customColorsIndex[3]
        settings.gridColorIndex = customColorsIndex[4]
        settings.barColorIndex = customColorsIndex[5]
        themeColorSelection()
    }

    Settings {
        id:settings
//        category: "MainApp"
        /* flag to show or not the App configuration popup */
//        property bool isFirstUse : true;
//        property bool isDaemonInstalled : false;
        property double maxScaleY : 0;
        property double maxScaleYStored : 0;
        property double maxScaleYSIM : 0;
        property double maxScaleYSIMStored : 0;
        property double axisX : 0;
        property double axisY : 0;
        property double errorX : 0;
        property double errorY1 : 0;
        property double errorY2 : 0;
        property string dataType : "SIM";

        property bool themeSelection : true;
        property bool themeSwitchByTime : false;
        property string backgrndColor: "#a9a9a9";
        property string fontColor: "#ffff00";
        property string gridColor: "#000000";
        property string lineColor: "#ffff00";
        property string buttonColor: "#606060";
        property string barColor: "#ffff00";
        property string backgrndColorCust: "#a9a9a9";
        property string fontColorCust: "#ffff33";
        property string gridColorCust: "#000000";
        property string lineColorCust: "#ffff33";
        property string buttonColorCust: "#000000";
        property string barColorCust: "#ff00ff";
        property real opac1: 1;
        property real opac2: 1;
        property real opac3: 0.15;
        property real opac4: 1;
        property real opac5: 1;
        property real opac6: 0.3;

        property int backgroundColorIndex: 2
        property int fontColorIndex: 7
        property int gridColorIndex: 0
        property int lineColorIndex: 7
        property int buttonColorIndex: 0
        property int barColorIndex: 4

        property real enableDaemonTime: 0;
        property real daemonIdleTime: 40;
        property string labelTime: "40 sec";
        property bool lockedPalette: true;
        property var daylightSwitchDate: new Date("1970-01-01T18:00:00");
        property string daylightSwitch: "18 00";
        property var wifiChartDate: new Date()
        property var simChartDate: new Date()
    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    Component {
       id: settingPage
       PrefPage{}
    }

    Component {
       id: aboutPage
       AboutPage{}
    }

    Component {
       id: chartPage
       ChartPage{}
    }

    PageStack {
        id: pageStack

        /* set the first page of the application */
        Component.onCompleted: {
            pageStack.push(mainPage);
        }

        Page {
            id: mainPage
            anchors.fill: parent

            header: PageHeader {
                id: pageHeader
                title: i18n.tr('Data usage monitor')

                trailingActionBar.actions: [
                Action {
                    text: i18n.tr("Settings")
                    onTriggered: pageStack.push(settingPage)
                    iconName: "settings"
                },
                Action {
                    text: i18n.tr("About")
                    onTriggered: pageStack.push(aboutPage)
                    iconName: "info"
                }
                ]
                StyleHints	{
                    foregroundColor: fontColor
                    backgroundColor: bkgColor
                    dividerColor: lineColor
                }
            }
            Component.onCompleted: {
                // if(!settings.isDaemonInstalled) {
                if(!Installdaemon.isInstalled) {
                    Installdaemon.install();
                    // if(Installdaemon.isInstalled) {
                    //     settings.isDaemonInstalled=false;
                    // }
                    // else {
                    //     settings.isDaemonInstalled=true;
                    // }
                }
                themeColorSelection();
                todayStoredSIM = todayData("SIM").toFixed(1)
                totalStoredSIM = totalData("SIM").toFixed(1);
                todayStoredWIFI = todayData("WIFI").toFixed(1);
                totalStoredWIFI = totalData("WIFI").toFixed(1);
            }

            Rectangle {
                color: bkgColor
                width: parent.width
                height: parent.height
            }

            ScrollView {
                id: scrollView
                anchors {
                    top: mainPage.header.bottom
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }

                Column {
                    width: scrollView.width
                    ListItem {
                        height: todayStoredSIMLayout.height + divider.height
                        divider.colorFrom: lineColor
                        ListItemLayout {
                            id: todayStoredSIMLayout
                            title {
                                text: i18n.tr("Today received data")
                                color: fontColor
                            }
                            subtitle {
                                text: i18n.tr("in MBytes")
                                color: fontColor
                                opacity: 0.8
                            }
                            Label {
                                text: todayStoredSIM
                                textSize: Label.Large
                                color: fontColor
                                SlotsLayout.position: SlotsLayout.Trailing
                            }
                        }
                        TextMetrics {
                            id: textMetrics1
                            text: todayStoredSIM
                        }
                    }
                    ListItem {
                        height: totalStoredSIMLayout.height + divider.height
                        divider.colorFrom: lineColor
                        ListItemLayout {
                            id: totalStoredSIMLayout
                            title {
                                text: i18n.tr("Month total received data")
                                color: fontColor
                            }
                            subtitle {
                                text: i18n.tr("in MBytes")
                                color: fontColor
                                opacity: 0.8
                            }
                            Label {
                                text: totalStoredSIM
                                textSize: Label.Large
                                color: fontColor
                                SlotsLayout.position: SlotsLayout.Trailing
                            }
                        }
                        TextMetrics {
                            id: textMetrics2
                            text: totalStoredSIM
                        }
                    }
                    ListItem {
                        height: simChart.height + divider.height
                        divider.colorFrom: lineColor
                        ListItemLayout {
                            id: simChart
                            title {
                                color: fontColor
                                text: i18n.tr("SIM data chart")
                            }
                            ProgressionSlot {color: fontColor}
                        }
                        onClicked: {
                            settings.dataType = "SIM";
                            pageStack.push(chartPage, {"targetDate": Qt.formatDateTime(settings.simChartDate, "yyyy-MM-dd")})
                        }
                    }
                    ListItem {
                        height: todayStoredWIFILayout.height + divider.height
                        divider.colorFrom: lineColor
                        ListItemLayout {
                            id: todayStoredWIFILayout
                            title {
                                color: fontColor
                                text: i18n.tr("Today received data")
                            }
                            subtitle {
                                text: i18n.tr("in MBytes")
                                color: fontColor
                                opacity: 0.8
                            }
                            Label {
                                text: todayStoredWIFI
                                textSize: Label.Large
                                color: fontColor
                                SlotsLayout.position: SlotsLayout.Trailing
                            }
                        }
                        TextMetrics {
                            id: textMetrics3
                            text: todayStoredWIFI
                        }
                    }
                    ListItem {
                        height: totalStoredWIFILayout.height + divider.height
                        divider.colorFrom: lineColor
                        ListItemLayout {
                            id: totalStoredWIFILayout
                            title {
                                text: i18n.tr("Month total received data")
                                color: fontColor
                            }
                            subtitle {
                                text: i18n.tr("in MBytes")
                                color: fontColor
                                opacity: 0.8
                            }
                            Label {
                                text: totalStoredWIFI
                                textSize: Label.Large
                                color: fontColor
                                SlotsLayout.position: SlotsLayout.Trailing
                            }
                        }
                        TextMetrics {
                            id: textMetrics4
                            text: totalStoredWIFI
                        }
                    }
                    ListItem {
                        height: wifiChart.height + divider.height
                        divider.colorFrom: lineColor
                        ListItemLayout {
                            id: wifiChart
                            title {
                                text: i18n.tr("Wi-fi data chart")
                                color: fontColor
                            }
                            ProgressionSlot {color: fontColor}
                        }
                        onClicked: {
                            settings.dataType = "WIFI";
                            pageStack.push(chartPage, {"targetDate": Qt.formatDateTime(settings.wifiChartDate, "yyyy-MM-dd")})
                        }
                    }
                    ListItem {
                        height: message.height + divider.height
                        divider.colorFrom: lineColor
                        visible: message.title.text != ""
                        clip: true
                        ListItemLayout {
                            id: message
                        }
                    }
                }
            }
        }
    }
    Connections {
        target: Installdaemon

        onInstalled: {
            if (success) {
                message.title.text = i18n.tr("Daemon automatically installed.\nPlease reboot the phone to set the data usage monitoring application online.");
                message.title.color = UbuntuColors.green;
            }
            else {
                message.title.text = i18n.tr("Failed to install daemon");
                message.title.color = UbuntuColors.red;
            }
        }
    }

  function themeColorSelection() {
  if (settings.themeSwitchByTime) {
    nowTime = new Date()
    var time = Qt.formatDateTime(nowTime, "hh mm")
    if (time>=settings.daylightSwitch) {
      var selectedTheme="Ubuntu.Components.Themes.SuruDark"
    } else {
      var selectedTheme="Ubuntu.Components.Themes.Ambiance"
    }
  } else {
    var selectedTheme=globalTheme
  }

  switch(settings.themeSelection) {
  case true:
      switch(selectedTheme) {
      case "Ubuntu.Components.Themes.Ambiance":
            settings.backgrndColor = "white"
            settings.gridColor = Qt.rgba(0,0,0, 0.05)
            settings.fontColor = "#666"
            settings.lineColor = Qt.rgba(0,0,0, 0.3)
            settings.buttonColor = "#606060"
            settings.barColor = Qt.rgba(0.2,0.2,0.2, 0.5)
            break;
      case "Ubuntu.Components.Themes.SuruDark":
            settings.backgrndColor = "black"
            settings.gridColor = Qt.rgba(1,1,1, 0.1)
            settings.fontColor = "white"
            settings.lineColor = "white"
            settings.buttonColor = "#606060"
            settings.barColor = Qt.rgba(0.86,0.86,0.86, 0.5)
            break;
      case "Ubuntu.Components.Themes.SuruGradient":
            settings.backgrndColor = "purple"
            settings.gridColor = Qt.rgba(1,0,1, 0.1)
            settings.fontColor = Qt.rgba(1,1,0, 1)
            settings.lineColor = Qt.rgba(1,0,1, 1)
            settings.buttonColor = "#ffff33"
            settings.barColor = Qt.rgba(0.86,0.86,0.86, 0.5)
            break;
      }
      bkgColor = settings.backgrndColor
      gridColor = settings.gridColor
      fontColor = settings.fontColor
      lineColor = settings.lineColor
      buttonColor = settings.buttonColor
      barColor = settings.barColor
      break;
  case false:
      bkgColor = Qt.tint(theme.palette.normal.background, settings.backgrndColorCust)
      gridColor = settings.gridColorCust
      fontColor = settings.fontColorCust
      lineColor = settings.lineColorCust
      buttonColor = settings.buttonColorCust
      barColor = settings.barColorCust
      break;
  }
  }

  function todayData(conntype) {
    var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
    var yesterday = DateUtils.addDaysAndFormat(actual_date, -1)
    var yesterdayData = Storage.getBytesValueByDate(yesterday, conntype)
    var firstOfMonth = DateUtils.firstOfMonth(actual_date);
		var daysToFirst = DateUtils.getDifferenceInDays(firstOfMonth,actual_date);
    var todayData = totalData(conntype)
    if (daysToFirst==0) {
        yesterdayData = 0
    }
    var sumData = todayData-yesterdayData
    if (sumData<0) {
      sumData=0
    }
    return sumData
  }

  function totalData(conntype) {
    var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
    var todayTotalData = Storage.getBytesValueByDate(actual_date, conntype)
    if (todayTotalData==0) {
      var firstOfMonth = DateUtils.firstOfMonth(actual_date);
      var daysToFirst = DateUtils.getDifferenceInDays(firstOfMonth,actual_date);
      if (daysToFirst>0) {
        var yesterday = DateUtils.addDaysAndFormat(actual_date, -1)
        var yesterdayData = Storage.getBytesValueByDate(yesterday, conntype)
        todayTotalData=yesterdayData
      } else {
        todayTotalData=0
      }
    }
    return todayTotalData
  }
}
