import sys
import os
import json
import subprocess
import shlex
import logging
from subprocess import Popen, PIPE
from gi.repository import Gio
from gi.repository import GLib


import gettext
t = gettext.translation('dataMonitor', fallback=True, localedir='/opt/click.ubuntu.com/datamonitor.matteobellei/current/share/locale/')  # TODO don't hardcode this
_ = t.gettext

BUS_NAME = "com.mbellei.indicator.datamonitor"
BUS_OBJECT_PATH = "/com/mbellei/indicator/datamonitor"
BUS_OBJECT_PATH_PHONE = BUS_OBJECT_PATH + "/phone"

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

class dataMonitorIndicator(object):
    ROOT_ACTION = 'root'
    SETTINGS_ACTION = 'settings'
    MAIN_SECTION = 0

    config_fileSIM = "/home/phablet/.config/datamonitor.matteobellei/configSIM.json"  # TODO don't hardcode this
    config_fileWIFI = "/home/phablet/.config/datamonitor.matteobellei/configWIFI.json"  # TODO don't hardcode this
    refresh_sec = 60

    def __init__(self, bus):
        self.bus = bus
        self.action_group = Gio.SimpleActionGroup()
        self.menu = Gio.Menu()
        self.sub_menu = Gio.Menu()
        self.get_config()

    def get_config(self):
            try:
                f = open(self.config_fileWIFI)
                config_json = json.load(f)
                self.today_data_WIFI = float(config_json['today_data_WIFI'].strip())
                self.total_data_WIFI = float(config_json['total_data_WIFI'].strip())
                f.close()
            except:
                self.today_data_WIFI = 0
                self.total_data_WIFI = 0

            try:
                f = open(self.config_fileSIM)
                config_json = {}
                config_json = json.load(f)
                self.today_data_SIM = float(config_json['today_data_SIM'].strip())
                self.total_data_SIM = float(config_json['total_data_SIM'].strip())
                f.close()
            except:
                self.today_data_SIM = 0
                self.total_data_SIM = 0



    def settings_action_activated(self, action, data):
        logger.debug('settings_action_activated')
        subprocess.Popen(shlex.split('ubuntu-app-launch datamonitor.matteobellei_datamonitor_0.1.8'))

    def _setup_actions(self):
        root_action = Gio.SimpleAction.new_stateful(self.ROOT_ACTION, None, self.root_state())
        self.action_group.insert(root_action)

        settings_action = Gio.SimpleAction.new(self.SETTINGS_ACTION, None)
        settings_action.connect('activate', self.settings_action_activated)
        self.action_group.insert(settings_action)

    def _create_section(self):
        self.get_config()
        section = Gio.Menu()
        settings_menu_item = Gio.MenuItem.new(_('dataMonitor\'s data usage information: '))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('SIM DATA USAGE: '))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('Today received data: ' + str(self.today_data_SIM) + ' MBytes'))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('Total month received data: ' + str(self.total_data_SIM) + ' MBytes'))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('WI-FI DATA USAGE: '))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('Today received data: ' + str(self.today_data_WIFI) + ' MBytes'))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('Total month received data: ' + str(self.total_data_WIFI) + ' MBytes'))
        section.append_item(settings_menu_item)

        return section

    def _setup_menu(self):
        self.sub_menu.insert_section(self.MAIN_SECTION, 'dataMonitor', self._create_section())

        root_menu_item = Gio.MenuItem.new('dataMonitor', 'indicator.{}'.format(self.ROOT_ACTION))
        root_menu_item.set_attribute_value('x-canonical-type', GLib.Variant.new_string('com.canonical.indicator.root'))
        root_menu_item.set_submenu(self.sub_menu)
        self.menu.append_item(root_menu_item)

    def _update_menu(self):
        self.sub_menu.remove(self.MAIN_SECTION)
        self.sub_menu.insert_section(self.MAIN_SECTION, 'dataMonitor', self._create_section())
        return True


    def run(self):
#        self.get_config()
        self._setup_actions()
        self._setup_menu()

        self.bus.export_action_group(BUS_OBJECT_PATH, self.action_group)
        self.menu_export = self.bus.export_menu_model(BUS_OBJECT_PATH_PHONE, self.menu)

        GLib.timeout_add_seconds(self.refresh_sec, self._update_menu)
        self._update_menu()


    def root_state(self):
        vardict = GLib.VariantDict.new()
        vardict.insert_value('visible', GLib.Variant.new_boolean(True))
        vardict.insert_value('title', GLib.Variant.new_string(_('dataMonitor')))

        icon = Gio.ThemedIcon.new("nm-adhoc")

        vardict.insert_value('icon', icon.serialize())

        return vardict.end()

if __name__ == '__main__':
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(bus, 0, None, 'org.freedesktop.DBus', '/org/freedesktop/DBus', 'org.freedesktop.DBus', None)
    result = proxy.RequestName('(su)', BUS_NAME, 0x4)
    if result != 1:
        logger.critical('Error: Bus name is already taken')
        sys.exit(1)

    wi = dataMonitorIndicator(bus)
    wi.run()
    logger.debug('dataMonitor Indicator startup completed')
    GLib.MainLoop().run()
