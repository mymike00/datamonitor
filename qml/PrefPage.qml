import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Installdaemon 1.0
import Ubuntu.Components.Pickers 1.3
import QtQuick.Layouts 1.3

Page {
    id: prefPage

    property int numberColors: 9
    property int paletteSize: (scrollView.width/numberColors<100) ? scrollView.width/numberColors : 100
    property int maxVertFontSize: 30
    property int maxHorizFontSize: 34
    property real minOpc: 0.05
    property string color1: Qt.rgba(0,0,0)                      //black
    property string color2: Qt.rgba(0,0,1)                      //blue
    property string color3: Qt.rgba(0.376471,0.376471,0.376471) //grey
    property string color4: Qt.rgba(0,0.6,0)                    //green
    property string color5: Qt.rgba(1,0,0)                      //red
    property string color6: Qt.rgba(1,0,1)                      //fuchsia
    property string color7: Qt.rgba(0,1,1)                      //skyblue
    property string color8: Qt.rgba(1,1,0.2)                    //yellow
    property string color9: Qt.rgba(1,1,1)                      //white
    property string colorTracker: Qt.rgba(1,0.5,0)
    property string borderColorTracker: Qt.rgba(0,0,0.9)

    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Settings")

        StyleHints	{
            foregroundColor: fontColor
            backgroundColor: bkgColor
            dividerColor: lineColor
        }

    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    Component.onCompleted: {
        switcherThemes.checkedChanged()
    }

    ScrollView {
        id: scrollView
        anchors {
            top: pageHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        clip: true

        Column {
            width: scrollView.width
            ListItem {
                divider.colorFrom: lineColor
                height: enableThemesColorStyle.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableThemesColorStyle
                    title {
                        text: i18n.tr("Enable global Theme style from OS general settings")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Switch {
                        id: switcherThemes
                        checked: settings.themeSelection
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onCheckedChanged: {
                            settings.themeSelection = switcherThemes.checked
                            themeColumn.visible = !checked
                            enableThemeColorDayTime.visible = checked
                            root.themeColorSelection()
                        }
                    }

                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: enableThemeColorDayTime.visible ? enableThemeColorDayTime.height + divider.height : 0
                clip: true
                ListItemLayout {
                    id: enableThemeColorDayTime
                    height: Math.max(mainSlot.height, enableThemeColorDayTimeColumn.height) + padding.top + padding.bottom
                    title {
                        text: i18n.tr("Enable global Theme to follow the day light (SuruDark or Ambiance themes) by setting the time for the switch")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Column {
                        id:enableThemeColorDayTimeColumn
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        spacing: units.gu(1)
                        Switch {
                            id: switcherTimeTheme
                            checked: settings.themeSwitchByTime
                            anchors.horizontalCenter: parent.horizontalCenter
                            onCheckedChanged: {
                                targetTimeSelectorButton.enabled = checked
                                settings.themeSwitchByTime=switcherTimeTheme.checked
                                root.themeColorSelection()
                            }
                        }
                        Button {
                            id: targetTimeSelectorButton
                            color: buttonColor
                            text: settings.daylightSwitch
                            anchors.horizontalCenter: parent.horizontalCenter
                            enabled:  switcherTimeTheme.checked
                            onClicked: {
                                if (settings.daylightSwitchDate.getHours() != settings.daylightSwitch.substring(0,2))
                                settings.daylightSwitchDate.setHours(settings.daylightSwitch.substring(0,2))
                                PopupUtils.open(popoverTargetTimePicker, targetTimeSelectorButton)
                            }
                        }
                    }
                }
            }
            Component {
                id: popoverTargetTimePicker
                Popover {
                    id: popoverTimePicker
                    property alias date: timePicker.date

                    DatePicker {
                        id: timePicker
                        mode: "Hours|Minutes"
                        date: settings.daylightSwitchDate
                        /* when Datepicker is closed, is updated the date shown in the button */
                        Component.onDestruction: {
                            targetTimeSelectorButton.text = Qt.formatDateTime(timePicker.date, "hh mm")
                            settings.daylightSwitchDate=timePicker.date
                            settings.daylightSwitch=targetTimeSelectorButton.text
                            root.themeColorSelection()
                        }
                    }
                }
            }

            Column {
                id: themeColumn
                width: parent.width
                ListItem {
                    divider.colorFrom: lineColor
                    height: labelPalettes.height + divider.height
                    ListItemLayout {
                        id: labelPalettes
                        title {
                            color: fontColor
                            font.weight: Font.DemiBold
                            text: i18n.tr("Colors Palettes:")
                            maximumLineCount:2
                        }
                        Icon {
                            id: lockColors
                            name: settings.lockedPalette ? "lock" : "lock-broken"
                            height: units.gu(4)
                            width: height
                            color: fontColor
                            SlotsLayout.position: SlotsLayout.Trailing
                            MouseArea {
                                anchors.fill: parent
                                onClicked: settings.lockedPalette = !settings.lockedPalette
                            }
                        }
                    }
                }
                Repeater {
                    model: [
                    {
                        label: i18n.tr("Background color:"),
                        namePalette: "background"
                    },
                    {
                        label: i18n.tr("Font color:"),
                        namePalette: "font"
                    },
                    {
                        label: i18n.tr("Lines color:"),
                        namePalette: "line"
                    },
                    {
                        label: i18n.tr("Buttons color:"),
                        namePalette: "button"
                    },
                    {
                        label: i18n.tr("Grid color (Graph only):"),
                        namePalette: "grid"
                    },
                    {
                        label: i18n.tr("Bars fill color (Graph only):"),
                        namePalette: "bar"
                    }
                    ]
                    delegate: ListItem {
                        divider.colorFrom: lineColor
                        height: colorPickerLayout.height + divider.height
                        enabled: !settings.lockedPalette
                        opacity: enabled ? 1.0 : 0.6
                        SlotsLayout {
                            id: colorPickerLayout
                            property var firstRepeaterIndex: index
                            mainSlot: ColumnLayout {
                                Label {
                                    color: fontColor
                                    text: modelData.label
                                }
                                Flickable {
                                    Layout.fillWidth: true
                                    height: backgroundPalette.height
                                    contentWidth: backgroundPalette.width
                                    Row {
                                        id: backgroundPalette
                                        spacing: units.dp(10)
                                        Repeater {
                                            id: repeater1
                                            model: root.colors
                                            property int selectedIndex: root.customColorsIndex[colorPickerLayout.firstRepeaterIndex]
                                            property string namePalette: modelData.namePalette
                                            delegate: Item {
                                                height: childrenRect.height
                                                width: childrenRect.width
                                                Rectangle {
                                                    id: colorRect
                                                    width: units.gu(6)
                                                    height: width
                                                    color: modelData
                                                    opacity: sliderBkg.value
                                                    MouseArea {
                                                        anchors.fill: parent
                                                        onClicked: {
                                                            var c = colorRect.color
                                                            var i = colorPickerLayout.firstRepeaterIndex
                                                            root.customColors[i] = Qt.rgba(c.r, c.g, c.b, root.opacArray[i])
                                                            root.updateCustomColors()
                                                            root.customColorsIndex[i] = index
                                                            root.updateCustomColorsIndex()
                                                        }
                                                    }
                                                }
                                                Rectangle {
                                                    id: selectedRect
                                                    visible: repeater1.selectedIndex == index
                                                    anchors.verticalCenter: colorRect.verticalCenter
                                                    anchors.horizontalCenter: colorRect.horizontalCenter
                                                    width: colorRect.width/3
                                                    height: colorRect.width/3
                                                    radius: height/2
                                                    color: prefPage.colorTracker
                                                    border {
                                                        color: prefPage.borderColorTracker
                                                        width: units.dp(3)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                RowLayout {
                                    id: fontOpacity
                                    Layout.fillWidth: true
                                    spacing: units.gu(2)
                                    Label {
                                        color: fontColor
                                        text: i18n.tr("Opacity:")
                                    }
                                    Slider {
                                        id: sliderBkg
                                        Layout.fillWidth: true
                                        SlotsLayout.position: SlotsLayout.Trailing
                                        function formatValue(v) { return v.toFixed(2) }
                                        onValueChanged: {
                                            if (themeColumn.visible) {
                                                var c = root.colors[repeater1.selectedIndex]
                                                root.opacArray[index] = value
                                                root.updateOpac()
                                                root.customColors[index] = Qt.rgba(c.r, c.g, c.b, value)
                                                root.updateCustomColors()
                                            }
                                        }
                                        minimumValue: 0.05
                                        maximumValue: 1
                                        value: root.opacArray[index]
                                        live: true
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: daemonLabel.height + divider.height
                ListItemLayout {
                    id: daemonLabel
                    title {
                        color: fontColor
                        font.weight: Font.DemiBold
                        text: i18n.tr("Daemon Settings:")
                        maximumLineCount:2
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: enableDaemonIdleTime.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableDaemonIdleTime
                    title {
                        color: fontColor
                        text: i18n.tr("Set time step for daemon in idle activity")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 5
                    }
                    subtitle {
                        text: i18n.tr("the smaller the worse for battery drain")
                        color: fontColor
                        opacity: 0.8
                        maximumLineCount: 5
                    }
                    Label {
                        id: availTime
                        color: fontColor
                        width: Math.min(implicitWidth, enableDaemonIdleTime.width*0.28)
                        wrapMode: Text.WordWrap
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        text: settings.labelTime
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: sliderDaemonIdleTime.height
                contentItem.clip: false
                Slider {
                    id: sliderDaemonIdleTime
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: scrollView.width - units.gu(5)
                    function formatValue(v) {
                        if (v>=60) {
                            var initime = v/60
                            var min = Math.floor(initime)
                            var sec = (initime-min)*60
                            var secRnd = Math.floor(sec)
                            // \u00A0 -> non-breaking space char
                            // number of seconds and "sec" are always on the same line
                            var time = min + "\u00A0min and " + secRnd + "\u00A0sec"
                        } else {
                            var time = v.toFixed(0) + "\u00A0sec"
                        }
                        settings.labelTime = time
                        availTime.text = time
                        return time
                    }
                    onValueChanged: {
                        settings.daemonIdleTime =  Math.round(value)
                        var daemonStep = settings.daemonIdleTime
                        Installdaemon.storeTimeStep(String(daemonStep))
                    }
                    minimumValue: 10
                    maximumValue: 600
                    value: settings.daemonIdleTime
                    live: true
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: deleteDaemonLabel.height + divider.height
                ListItemLayout {
                    id: deleteDaemonLabel
                    title {
                        text: i18n.tr("Remove daemon to save battery. This action will put offline the monitoring of received data")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Button {
                        id: deleteDaemon
                        text: i18n.tr("Remove daemon")
                        color: theme.palette.normal.negative
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onClicked: {
                            // if(settings.isDaemonInstalled)
                            if (Installdaemon.isInstalled) {
                                message.visible = false;
                                Installdaemon.uninstall();
                                // if(Installdaemon.isInstalled) {
                                //     settings.isDaemonInstalled=true;
                                // }
                                // else {
                                //     settings.isDaemonInstalled=false;
                                // }
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: indicatorLabel.height + divider.height
                ListItemLayout {
                    id: indicatorLabel
                    title {
                        color: fontColor
                        text: i18n.tr("Install dataMonitor indicator to have the data usage monitoring always at your finger tap ")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                    }
                    Button {
                        id: indicatorButton
                        property bool installed: (typeof !Installdaemon.isIndInstalled === "boolean") ? !Installdaemon.isIndInstalled : true
                        text: installed ? i18n.tr("Install Indicator") : i18n.tr("Uninstall Indicator")
                        color: installed ? theme.palette.normal.positive : theme.palette.normal.negative
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onClicked: {
                            messageIndicator.visible = true;
                            if (installed) {
                                Installdaemon.installInd()
                            } else {
                                Installdaemon.uninstallInd()
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: messageIndicator.visible ? messageIndicator.height + divider.height : 0
                ListItemLayout {
                    id: messageIndicator
                    visible: false
                }
            }
        }
    }
    Connections {
        target: Installdaemon

        onUninstalled: {
            message.visible = true;
            if (success) {
                message.text = i18n.tr("Daemon files removed.\nDataMonitor is offline. Close and open again the 'dataMonitor' app to get daemon files automatically installed.");
                message.color = UbuntuColors.green;
            }
            else {
                message.text = i18n.tr("Failed to uninstall both the daemon files");
                message.color = UbuntuColors.red;
            }
        }
        onInstalledIndicator: {
            messageIndicator.visible = true;
            if (success) {
                messageIndicator.title.text = i18n.tr("Indicator successfully installed, please reboot");
                messageIndicator.title.color = UbuntuColors.green;
            }
            else {
                messageIndicator.title.text = i18n.tr("Failed to install indicator");
                messageIndicator.title.color = UbuntuColors.red;
            }
        }
        onUninstalledIndicator: {
            messageIndicator.visible = true;
            if (success) {
                messageIndicator.title.text = i18n.tr("Indicator successfully uninstalled, please reboot");
                messageIndicator.title.color = UbuntuColors.green;
            }
            else {
                messageIndicator.title.text = i18n.tr("Failed to uninstall indicator");
                messageIndicator.title.color = UbuntuColors.red;
            }
        }
    }

    function colorsBystrings(opacity, color) {
        return Qt.rgba(color.r, color.g, color.b, opacity)
    }
}
