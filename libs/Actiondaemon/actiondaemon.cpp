#include <QGuiApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QObject>
#include <iostream>
#include <fstream>
#include <QTime>
#include "networkdaemon.h"

using namespace std;

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    Networkdaemon networkConn;
    bool isOnline = false;
    QVariant prevBytes = 0;
    QVariant prevTodayBytes = 0;
    QVariant totBytes = 0;
    QVariant connectionType;
    QVariant tLastPos = 0;
    QVariant sumBytes = 0;
    QVariant subBytes = 0;
    QVariant todayMBytes = 0;
    QVariant totalTodayMBytes = 0;
    QVariant WFtotData = 0;
    QVariant SIMtotData = 0;
    QList<QVariant> whichConn;
    QString saveLastPos;
    QVariant nowIsTime;
    string saveLastPosStr;
    bool isRebooted = false;
    int todayPosition;
    string todayIsUnity8;
    int numConns;
    double todayMbytesDouble = 0;

    nowIsTime = QTime::currentTime();
    qDebug()<< nowIsTime.toString();
    fstream rebootedFile;
    rebootedFile.open(".config/datamonitor.matteobellei/rebooted.conf",ios::in);
    rebootedFile>>todayIsUnity8;
    QString todayIsUnity8Str = QString::fromStdString(todayIsUnity8);
    rebootedFile.close();
    fstream todayPosFile;
    todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",ios::in);
    if(!todayPosFile) {
      qDebug()<<"Error in reading todayPos.conf file..";
      todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);  // line needed to create the file for the first time
      todayPosFile<<"0";
      qDebug()<<"File .config/datamonitor.matteobellei/todayPos.conf created.";
    }
    todayPosFile.close();
    fstream todayPosFileSIM;
    todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",ios::in);
    if(!todayPosFileSIM) {
      qDebug()<<"Error in reading todayPosSIM.conf file..";
      todayPosFile.open(".config/datamonitor.matteobellei/todayPosSIM.conf",fstream::out);  // line needed to create the file for the first time
      todayPosFileSIM<<"0";
      qDebug()<<"File .config/datamonitor.matteobellei/todayPosSIM.conf created.";
    }
    todayPosFileSIM.close();
    QQmlEngine engine;
    engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
    QQmlComponent component(&engine,
        QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/Daemon.qml")));
    if (component.isError()) {
        qWarning() << component.errors();
    }
    QObject *object = component.create();
    if (todayIsUnity8Str=="unity8") {
        for (int i=0; i <= 1; ++i) {
                 if (i==0) {
                   connectionType="WIFI";
                 } else {
                   connectionType="SIM";
                 }
                 QMetaObject::invokeMethod(object, "createDatabase",
                                   Q_ARG(QVariant, connectionType));
                 QMetaObject::invokeMethod(object, "fillVacantDays",
                                   Q_RETURN_ARG(QVariant, prevBytes),
                                   Q_ARG(QVariant, connectionType));
                 if (prevBytes.isNull()) {
                    prevBytes=0;
                 }
                 qDebug()<< "Yesterday" << connectionType.toString() << "stored bytes:" << prevBytes.toDouble() << "MB";
                 QMetaObject::invokeMethod(object, "todayLastPos",
                                   Q_RETURN_ARG(QVariant, tLastPos),
                                   Q_ARG(QVariant, connectionType));
                 saveLastPos = tLastPos.toString();
                 saveLastPosStr = saveLastPos.toUtf8().constData();
                 if (i==0) {
                    todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);
                    todayPosFile<<saveLastPosStr;
                    todayPosFile.close();
                    WFtotData = prevBytes;
                  } else {
                    todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",fstream::out);
                    todayPosFileSIM<<saveLastPosStr;
                    todayPosFileSIM.close();
                    SIMtotData = prevBytes;
                  }
                  if (tLastPos!=0) {
                    isRebooted = true;
                  }
        }
        if (isRebooted == true) {
            qDebug() << "Phone re-booted today.";
        } else {
            qDebug() << "Phone first start-up: no data stored for today yet.";
            networkConn.trigIndicator(WFtotData.toDouble(), SIMtotData.toDouble());
        }
        rebootedFile.open(".config/datamonitor.matteobellei/rebooted.conf",fstream::out);
        rebootedFile<<"mattdaemon-service";
        rebootedFile.close();
    } else if (todayIsUnity8Str=="mattdaemon-service") {
         whichConn = networkConn.recBytes();
         int i=0;
         numConns = whichConn.size();
         while (i < numConns) {
                if (whichConn[i]=="WIFI") {
                  todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",ios::in);
                  todayPosFile>>todayPosition;
                } else if (whichConn[i]=="SIM") {
                  todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",ios::in);
                  todayPosFileSIM>>todayPosition;
                }
                QMetaObject::invokeMethod(object, "yesterdayData",
                                Q_RETURN_ARG(QVariant, prevBytes),
                                Q_ARG(QVariant, whichConn[i]));
                QMetaObject::invokeMethod(object, "todayLastPos",
                                  Q_RETURN_ARG(QVariant, tLastPos),
                                  Q_ARG(QVariant, whichConn[i]));
                if (todayPosition>0 & tLastPos!=0) {
                  QVariant todayPositionBack = todayPosition;
                  QMetaObject::invokeMethod(object, "todayPrevBytes",
                                Q_RETURN_ARG(QVariant, prevTodayBytes),
                                Q_ARG(QVariant, todayPositionBack),
                                Q_ARG(QVariant, whichConn[i]));
                  qDebug()<< "Today stored bytes for" << whichConn[i].toString() << "connection is:" << prevTodayBytes.toDouble() << "MB";
                  QMetaObject::invokeMethod(object, "subtractNumbers",
                                Q_RETURN_ARG(QVariant, subBytes),
                                Q_ARG(QVariant, prevTodayBytes),
                                Q_ARG(QVariant, prevBytes));
                } else {
                  if (tLastPos==0) {
                    qDebug()<< "Today there are no stored bytes yet for" << whichConn[i].toString() << "connection (set tolerance = 0.1 MB)";
                  }
                }
//                if (prevBytes==0 & todayPosition==0 & tLastPos==0) {
                if (prevBytes==0 & todayPosition==0) {
                   networkConn.resetNotification(whichConn[i]);
                   qDebug()<< "All fired notifications have been reset for the current month.";
                }
                QMetaObject::invokeMethod(object, "sumNumbers",
                              Q_RETURN_ARG(QVariant, sumBytes),
                              Q_ARG(QVariant, prevBytes),
                              Q_ARG(QVariant, subBytes));
                todayMBytes = whichConn[i+1];
                todayMbytesDouble = todayMBytes.toDouble()/1048576;
                todayMBytes = todayMbytesDouble;
                QMetaObject::invokeMethod(object, "storeNewData",
                              Q_RETURN_ARG(QVariant, totBytes),
//                              Q_ARG(QVariant, whichConn[i+1]),
                              Q_ARG(QVariant, todayMBytes),
                              Q_ARG(QVariant, sumBytes),
                              Q_ARG(QVariant, whichConn[i]));
               qDebug()<< "Today received Mbytes for" << whichConn[i].toString() << "connection is:" << todayMbytesDouble << "MB";
               qDebug()<< "Total received Mbytes for" << whichConn[i].toString() << "connection is:" << totBytes.toDouble() << "MB";
               networkConn.shootNotification(whichConn[i], totBytes);
               totalTodayMBytes = totBytes.toDouble()-prevBytes.toDouble();
//               networkConn.saveDataIndicator(whichConn[i].toString(), totalTodayMBytes.toString(), totBytes.toString());
               networkConn.saveDataIndicator(whichConn[i].toString(), totalTodayMBytes.toDouble(), totBytes.toDouble());
               i = i+2;
         }
         if (numConns==0) {
           qDebug()<< "Network connections not available.";
         }
    }
    delete object;
    return 0;
}
