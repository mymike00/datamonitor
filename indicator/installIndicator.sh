#!/bin/bash

set -e

mkdir -p /home/phablet/.config/upstart/
mkdir -p /home/phablet/.local/share/unity/indicators/

cp -v /opt/click.ubuntu.com/datamonitor.matteobellei/current/indicator/mbellei-indicator-datamonitor.conf /home/phablet/.config/upstart/
cp -v /opt/click.ubuntu.com/datamonitor.matteobellei/current/indicator/com.mbellei.indicator.datamonitor /home/phablet/.local/share/unity/indicators/

echo "indicator-datamonitor installed!"
