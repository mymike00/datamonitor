import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import "Storage.js" as Storage
import "DateUtils.js" as DateUtils

Item {

	function createDatabase(connType)
	{
     	Storage.createTables(connType);
  }

	function todayPrevBytes(todayPos,connType)
	{
			var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
			var todayBytes = Storage.getBytesValueByDateAndPos(actual_date, todayPos, connType);
			return todayBytes;
	}

	function todayLastPos(connType)
	{
			var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
			var todayPos = Storage.getTodayLastPosition(actual_date,connType);
			return todayPos;
  }

	function subtractNumbers(data1, data2)
	{
		var sum = data1 - data2;
		return sum;
  }

	function sumNumbers(data1, data2)
	{
		var sum = data1 + data2;
		return sum;
  }

	function storeNewData(data, lastData, connType)
	{
//		 var bytes = data/1048576;
//		 print("Received Mbytes: " + bytes + " MB")
     var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
     var readLastBytes = Storage.getBytesValueByDate(actual_date,connType);
//		 var totBytes = bytes + lastData;
		 var totBytes = data + lastData;
		 var sumData = totBytes - readLastBytes;
//		 if (totBytes>0 & sumData>0)
		 if (totBytes>0 & sumData>0.1) // <-----------a minimum tolerance of 0.1 MBytes is introduced to avoid too many write cycles in the database
		 {
		 		var error = Storage.insertBytesData(actual_date,totBytes,connType);
		 }
		 return totBytes;
	}

  function fillVacantDays(connType)
	{
    var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
		var firstOfMonth = DateUtils.firstOfMonth(actual_date);
		var daysToFirst = DateUtils.getDifferenceInDays(firstOfMonth,actual_date);
    var readLastBytes = 0;
		var counter = -1;
		var days = 0;
		if (daysToFirst>0) {
				while (readLastBytes == 0 & days<daysToFirst) {
						var prevDate = DateUtils.addDaysAndFormat(actual_date, counter);
						var readLastBytes = Storage.getBytesValueByDate(prevDate,connType);
						counter = counter -1;
					  days = days +1;
				}
		}
		if (days >= 2 & readLastBytes>0) {
			for (var i=1; i < days; i++) {
					var lastDate = DateUtils.addDaysAndFormat(prevDate, i);
					var error = Storage.insertBytesData(lastDate,readLastBytes,connType);
			}
		}
		var readBytesToday = Storage.getBytesValueByDate(actual_date,connType);
		if (readBytesToday == 0) {
			 var error = Storage.insertBytesData(actual_date,readLastBytes,connType);
		}
		return readLastBytes;
	}

	function yesterdayData(connType)
	{
		var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
		var firstOfMonth = DateUtils.firstOfMonth(actual_date);
		var daysToFirst = DateUtils.getDifferenceInDays(firstOfMonth,actual_date);
		var readLastBytes = 0;
		if (daysToFirst>0) {
			  var yesterday = DateUtils.addDaysAndFormat(actual_date, -1)
        readLastBytes = Storage.getBytesValueByDate(yesterday,connType)
		}
		return readLastBytes;
	}

}
