cmake_minimum_required(VERSION 3.5)

#INCLUDE_DIRECTORIES(/usr/include/glib-2.0 /usr/include/gdk-pixbuf-2.0 ${CMAKE_CURRENT_SOURCE_DIR}/glibDep)

set(STATLIB "Actiondaemon")

find_package(Qt5Core)

#find_library(NOTIFLIB NAMES libnotify.so)
#message("libnotify.so for armhf arch found in: " ${NOTIFLIB})

add_executable(${STATLIB} actiondaemon.cpp networkdaemon.cpp)
#target_link_libraries(${STATLIB} ${NOTIFLIB})
target_link_libraries(${STATLIB})

qt5_use_modules(${STATLIB} Qml Quick DBus)
